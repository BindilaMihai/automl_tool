
# Automated Machine Learning Tool

### :desktop_computer: Fully automated machine learning system that solves any classification or regression problem that is stored in a tabular format

This tool was developed because other tools only partially automate machine learning processes, generating solutions that are reusable only with additional processing.

One of the biggest differentiators is that this system can learn from textual features or data with many inconsistencies. 

## Features

- Advanced data cleaning and complete feature engineering
- Search for optimally preprocessed data instances for each model
- Training of various models
- Bayesian optimization
- Stacking ensemble
- Easy-to-reuse models
- Reports that explain the created solution
- Operable without programming knowledge

## Diagram

![Diagram of the system](./paper_presentation/system_diagram.png)

## Performance

:stopwatch: The results shown below were obtained after running the systems for only 30 minutes.

**Comparison of classification results with similar tools**

![Comparison of classification results with similar tools
](./paper_presentation/classification_results.png)

**Comparison of regression results with similar tools**

![Comparison of regression results with similar tools](./paper_presentation/regression_results.png)

**Positioning relative to human performance**

![Positioning relative to human performance](./paper_presentation/performance_relative_to_humans.png)

## Appendix

- The research paper [End-to-End Automated Machine Learning System for Supervised Learning Problems](https://ieeexplore.ieee.org/document/9733439) presents the system in detail.
- A complete presentation and a copy of the research paper can be found in [this directory](./paper_presentation/).
- This project represents the basis of [Odin AI](https://odin-ai.net/#/).

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)

